# Oracle SQL
`by Venkata Bhattaram / tinitiate.com`

## Oracle SQL Topics
* 01-Table-DDL.sql
* 02-Table-Insert.sql
* 03-SQL-Select-Basics.sql
* 04-SQL-Select-Joins.sql
* 05-SQL-Select-Distinct-GroupBy.sql
* 06-SQL-Select-Having.sql
* 07-SQL-Insert-Variations.sql
* 08-SQL-Update.sql
* 09-SQL-Delete.sql
* 10-SQL-PsuedoColumns.sql
* 11-SQL-Advanced-Techniques.sql
* 12-SQL-Hierarchical-Queries.sql

## Oracle SQL labs
* Sample Tables and Data for Employee DB
* Exercises
* Interview Questions for Oracle SQL
